import { h, render } from 'preact';
import { Provider } from 'unistore/preact';
import Router from './../router';
import './index.css';

import createStore from './../store';

const store =
	(window.__STATE__ && createStore(window.__STATE__)) ||
	createStore({ count: 0 });

const app = document.getElementById('root');

render(
	<Provider store={store}>
		<Router />
	</Provider>,
	app,
	app.lastChild
);
