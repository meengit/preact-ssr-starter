const express = require('express');
const { h } = require('preact');
const render = require('preact-render-to-string');
import { Provider } from 'unistore/preact';
const path = require('path');

import Router from './../router';
import createStore from './../store';

const app = express();

const HTMLShell = (html, state) => `
	<!DOCTYPE html>
	<html>
		<head>
			<meta charset="utf-8">
			<meta name="viewport" content="width=device-width, initial-scale=1">
			<title>SSR Preact App</title>
		</head>
		<body>
			<div id="app">${html}</div>
			<script>window.__STATE__=${JSON.stringify(state).replace(/<|>/g, '')}</script>
			<script type="text/javascript" src="./client.js"></script>
		</body>
	</html>`;

// TODO: Find a better solution to get the path
app.use(express.static(path.join(__dirname, './../../dist')));

app.get('**', (req, res) => {
	const store = createStore({ count: 0 });

	let state = store.getState();

	let html = render(
		<Provider store={store}>
			<Router />
		</Provider>
	);

	res.send(HTMLShell(html, state));
});

app.listen(4000);
