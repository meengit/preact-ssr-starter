import { h } from 'preact';
import Router from 'preact-router';

import { App } from './../components/App';
import { About } from './../components/About';

// TODO: Switch example to dynamic routes
export default () => (
	<Router>
		<App path="/" />
		<About path="/about" />
	</Router>
);
