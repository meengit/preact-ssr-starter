import { h } from 'preact';
import { About } from './About';

describe('About Component', () => {
	it('renders the wrapper', () => {
		const wrapper = shallow(<About />);
		expect(wrapper.find('div')).to.have.length(1);
	});
});
