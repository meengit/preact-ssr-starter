const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');

module.exports = (env = process.env.NODE_ENV) => {
	const devel = (env === 'development' && true) || false;

	return {
		entry: path.join(path.join(process.cwd(), './src/entry/client.js')),
		devtool: devel && 'source-map',
		mode: env,
		output: {
			path: path.join(process.cwd(), './dist'),
			filename: 'client.js'
		},
		module: {
			rules: [
				{
					test: /\.css$/,
					use: [
						'style-loader',
						{
							loader: 'css-loader',
							options: {
								importLoaders: 1
							}
						},
						{
							loader: 'postcss-loader',
							options: {
								config: {
									path: path.join(process.cwd(), './tools/')
								}
							}
						}
					]
				},
				{
					test: /\.jsx?/i,
					exclude: /node_modules/,
					loader: 'babel-loader',
					options: {}
				}
			]
		},
		plugins:
			(devel && [
				new HtmlWebpackPlugin({
					title: 'Preact SSR Starter',
					template: path.join(process.cwd(), './src/views/static.html')
				})
			]) ||
			[],
		devServer: {
			contentBase: path.join(process.cwd(), 'dist'),
			inline: true,
			hot: true,
			port: 9000
		}
	};
};
