# Preact SSR Starter

> *[A set of articles/blog posts](https://gitlab.com/meengit/preact-ssr-starter/wikis/Links) inspires this starter. A big thank you to their authors!*

## Stack

* Compiling/Transforming: Babel 7, PostCSS
* Module Bundler: Webpack 4
* Application: Preact X, Unistore, Express
* Testing: Mocha, Chai, jsdom, Enzyme, Sinon
* Others: ESLint, Stylelint, Prettier

## Releases

<https://gitlab.com/meengit/preact-ssr-starter/-/releases>

## Next release

<https://gitlab.com/meengit/preact-ssr-starter/-/boards/1407287?milestone_title=v0.3.0&>

## Develop

Run client (<http://localhost:9000>):

```bash
npm run develop
```

Run server (<http://localhost:4000>):

```bash
npm run develop:server
```

## Build 

Client: 

```bash
npm run build
```

Server:

Not yet … ***However, you have to build the client-side JavaScript before you can run the production.*** Because the server is looking for `build/client.js` to ship this to the browser.

## Production

```bash
npm start
```

## Test

```bash
npm test # or npm run test:watch
```

## Wiki

Yes, there is a Wiki. It is currently not yet more then initialized. However, more contents are following … <https://gitlab.com/meengit/preact-ssr-starter/wikis/home>

