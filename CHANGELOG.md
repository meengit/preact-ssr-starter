# Changelog

## v0.2.0

*November 12th, 2019*

* The project is now using `cross-env` to set the environment variables. 
* The Webpack setup supports now `mode`. However, the `devtool` is set to `source-map` for *development* (default is `eval`). 
* Switched Webpack development mode for client JavaScript from `webpack` in watch mode to `webpack-dev-server` and `html-webpack-plugin`
* New directory structure which bundles client and server entry scripts in `./src/entry`.
* The SublimeText Project supports now `debugger` and `FFP`. The npm scripts follow a new naming strategy. `npm start` is reserved for running the (server-side) application in production. The other scripts follow the convention `task` , basically for the client, and `task:server` for server scripts.


## v0.1.0

*October 7th, 2019*

The initial commit; based on the inspiral links listed [here](https://gitlab.com/meengit/preact-ssr-starter/wikis/Links).
